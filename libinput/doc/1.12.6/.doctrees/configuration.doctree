���o      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _config_options:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��config-options�uh0h]h:Kh hhhh8�9/home/whot/code/libinput/build/doc/user/configuration.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Configuration options�h]�h�Configuration options�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h�=Below is a list of configurable options exposed to the users.�h]�h�=Below is a list of configurable options exposed to the users.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh �hint���)��}�(h��Not all configuration options are available on all devices. Use
:ref:`libinput list-devices <libinput-list-devices>` to show the
configuration options for local devices.�h]�h�)��}�(h��Not all configuration options are available on all devices. Use
:ref:`libinput list-devices <libinput-list-devices>` to show the
configuration options for local devices.�h]�(h�@Not all configuration options are available on all devices. Use
�����}�(h�@Not all configuration options are available on all devices. Use
�h h�ub�sphinx.addnodes��pending_xref���)��}�(h�4:ref:`libinput list-devices <libinput-list-devices>`�h]�h �inline���)��}�(hh�h]�h�libinput list-devices�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h��refexplicit���	reftarget��libinput-list-devices��refdoc��configuration��refwarn��uh0h�h8hkh:K	h h�ubh�5 to show the
configuration options for local devices.�����}�(h�5 to show the
configuration options for local devices.�h h�ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K	h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h hnhhh8hkh:Nubh�)��}�(hX�  libinput's configuration interface is available to the caller only, not
directly to the user. Thus is is the responsibility of the caller to expose
the various options and how these options are exposed. For example, the
xf86-input-libinput driver exposes the options through X Input device
properties and xorg.conf.d options. See the `libinput(4)
<https://www.mankier.com/4/libinput>`_ man page for more details.�h]�(hXP  libinput’s configuration interface is available to the caller only, not
directly to the user. Thus is is the responsibility of the caller to expose
the various options and how these options are exposed. For example, the
xf86-input-libinput driver exposes the options through X Input device
properties and xorg.conf.d options. See the �����}�(hXN  libinput's configuration interface is available to the caller only, not
directly to the user. Thus is is the responsibility of the caller to expose
the various options and how these options are exposed. For example, the
xf86-input-libinput driver exposes the options through X Input device
properties and xorg.conf.d options. See the �h h�hhh8Nh:Nubh)��}�(h�3`libinput(4)
<https://www.mankier.com/4/libinput>`_�h]�h�libinput(4)�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]��name��libinput(4)��refuri��"https://www.mankier.com/4/libinput�uh0hh h�ubh^)��}�(h�%
<https://www.mankier.com/4/libinput>�h]�h!}�(h#]��
libinput-4�ah%]�h']��libinput(4)�ah)]�h+]��refuri�h�uh0h]�
referenced�Kh h�ubh� man page for more details.�����}�(h� man page for more details.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubhm)��}�(hhh]�(hr)��}�(h�Tap-to-click�h]�h�Tap-to-click�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:Kubh�)��}�(h�hSee :ref:`tapping` for details on how this feature works. Configuration
options exposed by libinput are:�h]�(h�See �����}�(h�See �h j  hhh8Nh:Nubh�)��}�(h�:ref:`tapping`�h]�h�)��}�(hj)  h]�h�tapping�����}�(hhh j+  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j'  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j5  �refexplicit��hÌtapping�h�h�hǈuh0h�h8hkh:Kh j  ubh�V for details on how this feature works. Configuration
options exposed by libinput are:�����}�(h�V for details on how this feature works. Configuration
options exposed by libinput are:�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�5how many tapping fingers are supported by this device�h]�h�)��}�(hjY  h]�h�5how many tapping fingers are supported by this device�����}�(hjY  h j[  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh jW  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jU  h jR  hhh8hkh:NubjV  )��}�(h�"a toggle to enable/disable tapping�h]�h�)��}�(hjp  h]�h�"a toggle to enable/disable tapping�����}�(hjp  h jr  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh jn  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jU  h jR  hhh8hkh:NubjV  )��}�(h�=a toggle to enable/disable tap-and-drag, see :ref:`tapndrag`.�h]�h�)��}�(hj�  h]�(h�-a toggle to enable/disable tap-and-drag, see �����}�(h�-a toggle to enable/disable tap-and-drag, see �h j�  ubh�)��}�(h�:ref:`tapndrag`�h]�h�)��}�(hj�  h]�h�tapndrag�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��hÌtapndrag�h�h�hǈuh0h�h8hkh:Kh j�  ubh�.�����}�(h�.�h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jU  h jR  hhh8hkh:NubjV  )��}�(h�Ea toggle to enable/disable tap-and-drag drag lock see :ref:`tapndrag`�h]�h�)��}�(hj�  h]�(h�6a toggle to enable/disable tap-and-drag drag lock see �����}�(h�6a toggle to enable/disable tap-and-drag drag lock see �h j�  ubh�)��}�(h�:ref:`tapndrag`�h]�h�)��}�(hj�  h]�h�tapndrag�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��hÌtapndrag�h�h�hǈuh0h�h8hkh:Kh j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jU  h jR  hhh8hkh:NubjV  )��}�(h��The default order is 1, 2, 3 finger tap mapping to left, right, middle
click, respectively. This order can be changed to left, middle, right click,
respectively.
�h]�h�)��}�(h��The default order is 1, 2, 3 finger tap mapping to left, right, middle
click, respectively. This order can be changed to left, middle, right click,
respectively.�h]�h��The default order is 1, 2, 3 finger tap mapping to left, right, middle
click, respectively. This order can be changed to left, middle, right click,
respectively.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jU  h jR  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0jP  h8hkh:Kh j  hhubh�)��}�(h��Tapping is usually available on touchpads and the touchpad part of external
graphics tablets. Tapping is usually **not** available on touch screens,
for those devices it is expected to be implemented by the toolkit.�h]�(h�qTapping is usually available on touchpads and the touchpad part of external
graphics tablets. Tapping is usually �����}�(h�qTapping is usually available on touchpads and the touchpad part of external
graphics tablets. Tapping is usually �h j  hhh8Nh:Nubh �strong���)��}�(h�**not**�h]�h�not�����}�(hhh j!  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  ubh�_ available on touch screens,
for those devices it is expected to be implemented by the toolkit.�����}�(h�_ available on touch screens,
for those devices it is expected to be implemented by the toolkit.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K$h j  hhubeh!}�(h#]��tap-to-click�ah%]�h']��tap-to-click�ah)]�h+]�uh0hlh hnhhh8hkh:Kubhm)��}�(hhh]�(hr)��}�(h�Send Events Mode�h]�h�Send Events Mode�����}�(hjG  h jE  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jB  hhh8hkh:K*ubh�)��}�(hX�  The Send Events Mode is libinput's terminology for disabling a device. It is
more precise in that the device only stops sending events but may not get
fully disabled. For example, disabling the touchpad on a
:ref:`Lenovo T440 and similar <t440_support>` leaves the top software
buttons enabled for the trackpoint. Available options are
**enabled** (send events normally), **disabled** ( do not send events),
**disabled on external mouse** (disable the device while an external mouse
is plugged in).�h]�(h��The Send Events Mode is libinput’s terminology for disabling a device. It is
more precise in that the device only stops sending events but may not get
fully disabled. For example, disabling the touchpad on a
�����}�(h��The Send Events Mode is libinput's terminology for disabling a device. It is
more precise in that the device only stops sending events but may not get
fully disabled. For example, disabling the touchpad on a
�h jS  hhh8Nh:Nubh�)��}�(h�-:ref:`Lenovo T440 and similar <t440_support>`�h]�h�)��}�(hj^  h]�h�Lenovo T440 and similar�����}�(hhh j`  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j\  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jj  �refexplicit��hÌt440_support�h�h�hǈuh0h�h8hkh:K,h jS  ubh�S leaves the top software
buttons enabled for the trackpoint. Available options are
�����}�(h�S leaves the top software
buttons enabled for the trackpoint. Available options are
�h jS  hhh8Nh:Nubj   )��}�(h�**enabled**�h]�h�enabled�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h jS  ubh� (send events normally), �����}�(h� (send events normally), �h jS  hhh8Nh:Nubj   )��}�(h�**disabled**�h]�h�disabled�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h jS  ubh� ( do not send events),
�����}�(h� ( do not send events),
�h jS  hhh8Nh:Nubj   )��}�(h�**disabled on external mouse**�h]�h�disabled on external mouse�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h jS  ubh�< (disable the device while an external mouse
is plugged in).�����}�(h�< (disable the device while an external mouse
is plugged in).�h jS  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K,h jB  hhubh^)��}�(h� .. _config_pointer_acceleration:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�config-pointer-acceleration�uh0h]h:K;h jB  hhh8hkubeh!}�(h#]��send-events-mode�ah%]�h']��send events mode�ah)]�h+]�uh0hlh hnhhh8hkh:K*ubhm)��}�(hhh]�(hr)��}�(h�Pointer acceleration�h]�h�Pointer acceleration�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K:ubh�)��}�(h��Pointer acceleration is a function to convert input deltas to output deltas,
usually based on the movement speed of the device, see
:ref:`pointer-acceleration` for details.�h]�(h��Pointer acceleration is a function to convert input deltas to output deltas,
usually based on the movement speed of the device, see
�����}�(h��Pointer acceleration is a function to convert input deltas to output deltas,
usually based on the movement speed of the device, see
�h j�  hhh8Nh:Nubh�)��}�(h�:ref:`pointer-acceleration`�h]�h�)��}�(hj�  h]�h�pointer-acceleration�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��hÌpointer-acceleration�h�h�hǈuh0h�h8hkh:K<h j�  ubh� for details.�����}�(h� for details.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K<h j�  hhubh�)��}�(h��Pointer acceleration is normalized into a [-1, 1] range, where -1 is
"slowest" and 1 is "fastest". Most devices use a default speed of 0.�h]�h��Pointer acceleration is normalized into a [-1, 1] range, where -1 is
“slowest” and 1 is “fastest”. Most devices use a default speed of 0.�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K@h j�  hhubh�)��}�(h��The pointer acceleration profile defines **how** the input deltas are
converted, see :ref:`ptraccel-profiles`. Most devices have their default
profile (usually called "adaptive") and a "flat" profile. The flat profile
does not apply any acceleration.�h]�(h�)The pointer acceleration profile defines �����}�(h�)The pointer acceleration profile defines �h j"  hhh8Nh:Nubj   )��}�(h�**how**�h]�h�how�����}�(hhh j+  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j"  ubh�% the input deltas are
converted, see �����}�(h�% the input deltas are
converted, see �h j"  hhh8Nh:Nubh�)��}�(h�:ref:`ptraccel-profiles`�h]�h�)��}�(hj@  h]�h�ptraccel-profiles�����}�(hhh jB  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j>  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jL  �refexplicit��hÌptraccel-profiles�h�h�hǈuh0h�h8hkh:KCh j"  ubh��. Most devices have their default
profile (usually called “adaptive”) and a “flat” profile. The flat profile
does not apply any acceleration.�����}�(h��. Most devices have their default
profile (usually called "adaptive") and a "flat" profile. The flat profile
does not apply any acceleration.�h j"  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KCh j�  hhubeh!}�(h#]�(�pointer-acceleration�j�  eh%]�h']�(�pointer acceleration��config_pointer_acceleration�eh)]�h+]�uh0hlh hnhhh8hkh:K:�expect_referenced_by_name�}�jm  j�  s�expect_referenced_by_id�}�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�	Scrolling�h]�h�	Scrolling�����}�(hjy  h jw  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jt  hhh8hkh:KJubh�)��}�(hX#  "Natural scrolling" is the terminology for moving the content in the
direction of scrolling, i.e. moving the wheel or fingers down moves the page
down. Traditional scrolling moves the content in the opposite direction.
Natural scrolling can be turned on or off, it is usually off by default.�h]�hX'  “Natural scrolling” is the terminology for moving the content in the
direction of scrolling, i.e. moving the wheel or fingers down moves the page
down. Traditional scrolling moves the content in the opposite direction.
Natural scrolling can be turned on or off, it is usually off by default.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KLh jt  hhubh�)��}�(hXY  The scroll method defines how to trigger scroll events. On touchpads
libinput provides two-finger scrolling and edge scrolling. Two-finger
scrolling converts a movement with two fingers to a series of scroll events.
Edge scrolling converts a movement with one finger along the right or bottom
edge of the touchpad into a series of scroll events.�h]�hXY  The scroll method defines how to trigger scroll events. On touchpads
libinput provides two-finger scrolling and edge scrolling. Two-finger
scrolling converts a movement with two fingers to a series of scroll events.
Edge scrolling converts a movement with one finger along the right or bottom
edge of the touchpad into a series of scroll events.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KQh jt  hhubh�)��}�(h��On other libinput provides button-scrolling - movement of the device while
the designated scroll button is down is converted to scroll events. The
button used for scrolling is configurable.�h]�h��On other libinput provides button-scrolling - movement of the device while
the designated scroll button is down is converted to scroll events. The
button used for scrolling is configurable.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KWh jt  hhubh�)��}�(hX#  The scroll method can be chosen or disabled altogether but most devices only
support a subset of available scroll methods. libinput's default is
two-finger scrolling for multi-touch touchpads, edge scrolling for
single-touch touchpads. On tracksticks, button scrolling is enabled by
default.�h]�hX%  The scroll method can be chosen or disabled altogether but most devices only
support a subset of available scroll methods. libinput’s default is
two-finger scrolling for multi-touch touchpads, edge scrolling for
single-touch touchpads. On tracksticks, button scrolling is enabled by
default.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K[h jt  hhubh�)��}�(h�ESee :ref:`scrolling` for more details on how the scroll methods work.�h]�(h�See �����}�(h�See �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`scrolling`�h]�h�)��}�(hj�  h]�h�	scrolling�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��hÌ	scrolling�h�h�hǈuh0h�h8hkh:Kah j�  ubh�1 for more details on how the scroll methods work.�����}�(h�1 for more details on how the scroll methods work.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kah jt  hhubeh!}�(h#]��	scrolling�ah%]�h']��	scrolling�ah)]�h+]�uh0hlh hnhhh8hkh:KJubhm)��}�(hhh]�(hr)��}�(h�Left-handed Mode�h]�h�Left-handed Mode�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Keubh�)��}�(hXE  Left-handed mode switches the device's functionality to be more
accommodating for left-handed users. On mice this usually means swapping the
left and right mouse button, on tablets this allows the tablet to be used
upside-down to present the pad buttons for the non-dominant right hand. Not
all devices have left-handed mode.�h]�hXG  Left-handed mode switches the device’s functionality to be more
accommodating for left-handed users. On mice this usually means swapping the
left and right mouse button, on tablets this allows the tablet to be used
upside-down to present the pad buttons for the non-dominant right hand. Not
all devices have left-handed mode.�����}�(hj
  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kgh j�  hhubh�)��}�(h�GLeft-handed mode can be enabled or disabled and is disabled by default.�h]�h�GLeft-handed mode can be enabled or disabled and is disabled by default.�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kmh j�  hhubeh!}�(h#]��left-handed-mode�ah%]�h']��left-handed mode�ah)]�h+]�uh0hlh hnhhh8hkh:Keubhm)��}�(hhh]�(hr)��}�(h�Middle Button Emulation�h]�h�Middle Button Emulation�����}�(hj1  h j/  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j,  hhh8hkh:Kqubh�)��}�(h��Middle button emulation converts a simultaneous left and right button click
into a middle button. The emulation can be enabled or disabled. Middle
button emulation is usually enabled when the device does not provide a
middle button.�h]�h��Middle button emulation converts a simultaneous left and right button click
into a middle button. The emulation can be enabled or disabled. Middle
button emulation is usually enabled when the device does not provide a
middle button.�����}�(hj?  h j=  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Ksh j,  hhubeh!}�(h#]��middle-button-emulation�ah%]�h']��middle button emulation�ah)]�h+]�uh0hlh hnhhh8hkh:Kqubhm)��}�(hhh]�(hr)��}�(h�Click method�h]�h�Click method�����}�(hjX  h jV  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jS  hhh8hkh:Kzubh�)��}�(hX	  The click method defines how button events are triggered on a :ref:`clickpad
<clickpad_softbuttons>`. When set to button areas, the bottom area of the
touchpad is divided into a left, middle and right button area. When set to
clickfinger, the number of fingers on the touchpad decide the button type.
Clicking with 1, 2, 3 fingers triggers a left, right, or middle click,
respectively. The default click method is software button areas. Click
methods are usually only available on :ref:`clickpads
<clickpad_softbuttons>`.�h]�(h�>The click method defines how button events are triggered on a �����}�(h�>The click method defines how button events are triggered on a �h jd  hhh8Nh:Nubh�)��}�(h�&:ref:`clickpad
<clickpad_softbuttons>`�h]�h�)��}�(hjo  h]�h�clickpad�����}�(hhh jq  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h jm  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j{  �refexplicit��hÌclickpad_softbuttons�h�h�hǈuh0h�h8hkh:K|h jd  ubhX}  . When set to button areas, the bottom area of the
touchpad is divided into a left, middle and right button area. When set to
clickfinger, the number of fingers on the touchpad decide the button type.
Clicking with 1, 2, 3 fingers triggers a left, right, or middle click,
respectively. The default click method is software button areas. Click
methods are usually only available on �����}�(hX}  . When set to button areas, the bottom area of the
touchpad is divided into a left, middle and right button area. When set to
clickfinger, the number of fingers on the touchpad decide the button type.
Clicking with 1, 2, 3 fingers triggers a left, right, or middle click,
respectively. The default click method is software button areas. Click
methods are usually only available on �h jd  hhh8Nh:Nubh�)��}�(h�':ref:`clickpads
<clickpad_softbuttons>`�h]�h�)��}�(hj�  h]�h�	clickpads�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��hÌclickpad_softbuttons�h�h�hǈuh0h�h8hkh:K|h jd  ubh�.�����}�(hj�  h jd  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K|h jS  hhubeh!}�(h#]��click-method�ah%]�h']��click method�ah)]�h+]�uh0hlh hnhhh8hkh:Kzubhm)��}�(hhh]�(hr)��}�(h�Disable while typing�h]�h�Disable while typing�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(h��DWT is the most generic form of palm detection on touchpad. While the user
is typing the touchpad is disabled, the touchpad is enabled after a timeout.
See :ref:`disable-while-typing` for more info.�h]�(h��DWT is the most generic form of palm detection on touchpad. While the user
is typing the touchpad is disabled, the touchpad is enabled after a timeout.
See �����}�(h��DWT is the most generic form of palm detection on touchpad. While the user
is typing the touchpad is disabled, the touchpad is enabled after a timeout.
See �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`disable-while-typing`�h]�h�)��}�(hj�  h]�h�disable-while-typing�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��hÌdisable-while-typing�h�h�hǈuh0h�h8hkh:K�h j�  ubh� for more info.�����}�(h� for more info.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(h�\Disable-while-typing can be enabled or disabled, it is enabled by default on
most touchpads.�h]�h�\Disable-while-typing can be enabled or disabled, it is enabled by default on
most touchpads.�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubeh!}�(h#]��disable-while-typing�ah%]�h']��disable while typing�ah)]�h+]�uh0hlh hnhhh8hkh:K�ubhm)��}�(hhh]�(hr)��}�(h�Calibration�h]�h�Calibration�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:K�ubh�)��}�(hXW  Calibration is available for some direct-input devices (touch screens,
graphics tablets, etc.). The purpose of calibration is to ensure the input
lines up with the output and the configuration data is a transformation
matrix. It is thus not expected that the user sets this option. The desktop
environment should provide an interface for this.�h]�hXW  Calibration is available for some direct-input devices (touch screens,
graphics tablets, etc.). The purpose of calibration is to ensure the input
lines up with the output and the configuration data is a transformation
matrix. It is thus not expected that the user sets this option. The desktop
environment should provide an interface for this.�����}�(hj,  h j*  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j  hhubeh!}�(h#]��calibration�ah%]�h']��calibration�ah)]�h+]�uh0hlh hnhhh8hkh:K�ubhm)��}�(hhh]�(hr)��}�(h�Rotation�h]�h�Rotation�����}�(hjE  h jC  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j@  hhh8hkh:K�ubh�)��}�(hX6  The device rotation applies a corrective angle to relative input events.
This is currently only available on trackpoints which may be used sideways
or upside-down. The angle can be freely chosen but not all devices support
rotation other than 0, 90, 180, or 270 degrees. Rotation is off (0 degrees)
by default.�h]�hX6  The device rotation applies a corrective angle to relative input events.
This is currently only available on trackpoints which may be used sideways
or upside-down. The angle can be freely chosen but not all devices support
rotation other than 0, 90, 180, or 270 degrees. Rotation is off (0 degrees)
by default.�����}�(hjS  h jQ  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j@  hhubeh!}�(h#]��rotation�ah%]�h']��rotation�ah)]�h+]�uh0hlh hnhhh8hkh:K�ubeh!}�(h#]�(�configuration-options�hjeh%]�h']�(�configuration options��config_options�eh)]�h+]�uh0hlh hhhh8hkh:Kjp  }�jm  h_sjr  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj�  ]�j�  au�nameids�}�(jm  hjjl  ji  h�h�j?  j<  j�  j�  jm  j�  jl  ji  j�  j�  j)  j&  jP  jM  j�  j�  j  j  j=  j:  jd  ja  u�	nametypes�}�(jm  �jl  Nh��j?  Nj�  Njm  �jl  Nj�  Nj)  NjP  Nj�  Nj  Nj=  Njd  Nuh#}�(hjhnji  hnh�h�j<  j  j�  jB  j�  j�  ji  j�  j�  jt  j&  j�  jM  j,  j�  jS  j  j�  j:  j  ja  j@  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "config-options" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�AHyperlink target "config-pointer-acceleration" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�K;uh0j�  ube�transformer�N�
decoration�Nhhub.
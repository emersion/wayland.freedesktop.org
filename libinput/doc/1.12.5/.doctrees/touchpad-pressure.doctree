��?>      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`7d25d9e`�h]�h �	reference���)��}�(h�git commit 7d25d9e�h]�h �Text����git commit 7d25d9e�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/7d25d9e�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4b9dfe32f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�h]�h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4b9dfe32f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _touchpad_pressure:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��touchpad-pressure�uh0h]h:Kh hhhh8�=/home/whot/code/libinput/build/doc/user/touchpad-pressure.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�'Touchpad pressure-based touch detection�h]�h�'Touchpad pressure-based touch detection�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hX�  libinput uses the touchpad pressure values and/or touch size values to
detect whether a finger has been placed on the touchpad. This is
:ref:`kernel_pressure_information` and combines with a libinput-specific hardware
database to adjust the thresholds on a per-device basis. libinput uses
these thresholds primarily to filter out accidental light touches but
the information is also used for some :ref:`palm_detection`.�h]�(h��libinput uses the touchpad pressure values and/or touch size values to
detect whether a finger has been placed on the touchpad. This is
�����}�(h��libinput uses the touchpad pressure values and/or touch size values to
detect whether a finger has been placed on the touchpad. This is
�h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�":ref:`kernel_pressure_information`�h]�h �inline���)��}�(hh�h]�h�kernel_pressure_information�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h��refexplicit���	reftarget��kernel_pressure_information��refdoc��touchpad-pressure��refwarn��uh0h�h8hkh:Kh h�ubh�� and combines with a libinput-specific hardware
database to adjust the thresholds on a per-device basis. libinput uses
these thresholds primarily to filter out accidental light touches but
the information is also used for some �����}�(h�� and combines with a libinput-specific hardware
database to adjust the thresholds on a per-device basis. libinput uses
these thresholds primarily to filter out accidental light touches but
the information is also used for some �h h�hhh8Nh:Nubh�)��}�(h�:ref:`palm_detection`�h]�h�)��}�(hh�h]�h�palm_detection�����}�(hhh h�ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�hǌrefexplicit��h��palm_detection�h�h�h��uh0h�h8hkh:Kh h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX  Most devices only support one of either touch pressure or touch size.
libinput uses whichever is available but a preference is given to touch size
as it provides more specific information. Since most devices only provide
one type anyway, this internal preference does not usually matter.�h]�hX  Most devices only support one of either touch pressure or touch size.
libinput uses whichever is available but a preference is given to touch size
as it provides more specific information. Since most devices only provide
one type anyway, this internal preference does not usually matter.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hXS  Pressure and touch size thresholds are **not** directly configurable by the
user. Instead, libinput provides these thresholds for each device where
necessary. See :ref:`touchpad_pressure_hwdb` for instructions on how to adjust
the pressure ranges and :ref:`touchpad_touch_size_hwdb` for instructions on
how to adjust the touch size ranges.�h]�(h�'Pressure and touch size thresholds are �����}�(h�'Pressure and touch size thresholds are �h h�hhh8Nh:Nubh �strong���)��}�(h�**not**�h]�h�not�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�u directly configurable by the
user. Instead, libinput provides these thresholds for each device where
necessary. See �����}�(h�u directly configurable by the
user. Instead, libinput provides these thresholds for each device where
necessary. See �h h�hhh8Nh:Nubh�)��}�(h�:ref:`touchpad_pressure_hwdb`�h]�h�)��}�(hj  h]�h�touchpad_pressure_hwdb�����}�(hhh j  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��h��touchpad_pressure_hwdb�h�h�h��uh0h�h8hkh:Kh h�ubh�; for instructions on how to adjust
the pressure ranges and �����}�(h�; for instructions on how to adjust
the pressure ranges and �h h�hhh8Nh:Nubh�)��}�(h�:ref:`touchpad_touch_size_hwdb`�h]�h�)��}�(hj3  h]�h�touchpad_touch_size_hwdb�����}�(hhh j5  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j1  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j?  �refexplicit��h��touchpad_touch_size_hwdb�h�h�h��uh0h�h8hkh:Kh h�ubh�9 for instructions on
how to adjust the touch size ranges.�����}�(h�9 for instructions on
how to adjust the touch size ranges.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h� .. _kernel_pressure_information:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�kernel-pressure-information�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�"Information provided by the kernel�h]�h�"Information provided by the kernel�����}�(hjj  h jh  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh je  hhh8hkh:Kubh�)��}�(hX  The kernel sends multiple values to inform userspace about a finger touching
the touchpad. The most basic is the ``EV_KEY/BTN_TOUCH`` boolean event
that simply announces physical contact with the touchpad. The decision when
this event is sent is usually made by the kernel driver and may depend on
device-specific thresholds. These thresholds are transparent to userspace
and cannot be modified. On touchpads where pressure or touch size is not
available, libinput uses ``BTN_TOUCH`` to determine when a finger is
logically down.�h]�(h�qThe kernel sends multiple values to inform userspace about a finger touching
the touchpad. The most basic is the �����}�(h�qThe kernel sends multiple values to inform userspace about a finger touching
the touchpad. The most basic is the �h jv  hhh8Nh:Nubh �literal���)��}�(h�``EV_KEY/BTN_TOUCH``�h]�h�EV_KEY/BTN_TOUCH�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h jv  ubhXQ   boolean event
that simply announces physical contact with the touchpad. The decision when
this event is sent is usually made by the kernel driver and may depend on
device-specific thresholds. These thresholds are transparent to userspace
and cannot be modified. On touchpads where pressure or touch size is not
available, libinput uses �����}�(hXQ   boolean event
that simply announces physical contact with the touchpad. The decision when
this event is sent is usually made by the kernel driver and may depend on
device-specific thresholds. These thresholds are transparent to userspace
and cannot be modified. On touchpads where pressure or touch size is not
available, libinput uses �h jv  hhh8Nh:Nubj�  )��}�(h�``BTN_TOUCH``�h]�h�	BTN_TOUCH�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h jv  ubh�. to determine when a finger is
logically down.�����}�(h�. to determine when a finger is
logically down.�h jv  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh je  hhubh�)��}�(hX�  Many contemporary touchpad devices provide an absolute pressure axis in
addition to ``BTN_TOUCH``. This pressure generally increases as the pressure
increases, however few touchpads are capable of detecting true pressure. The
pressure value is usually related to the covered area - as the pressure
increases a finger flattens and thus covers a larger area. The range
provided by the kernel is not mapped to a specific physical range and
often requires adjustment. Pressure is sent by the ``ABS_PRESSURE`` axis
for single-touch touchpads or ``ABS_MT_PRESSURE`` on multi-touch capable
touchpads. Some devices can detect multiple fingers but only provide
``ABS_PRESSURE``.�h]�(h�TMany contemporary touchpad devices provide an absolute pressure axis in
addition to �����}�(h�TMany contemporary touchpad devices provide an absolute pressure axis in
addition to �h j�  hhh8Nh:Nubj�  )��}�(h�``BTN_TOUCH``�h]�h�	BTN_TOUCH�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubhX�  . This pressure generally increases as the pressure
increases, however few touchpads are capable of detecting true pressure. The
pressure value is usually related to the covered area - as the pressure
increases a finger flattens and thus covers a larger area. The range
provided by the kernel is not mapped to a specific physical range and
often requires adjustment. Pressure is sent by the �����}�(hX�  . This pressure generally increases as the pressure
increases, however few touchpads are capable of detecting true pressure. The
pressure value is usually related to the covered area - as the pressure
increases a finger flattens and thus covers a larger area. The range
provided by the kernel is not mapped to a specific physical range and
often requires adjustment. Pressure is sent by the �h j�  hhh8Nh:Nubj�  )��}�(h�``ABS_PRESSURE``�h]�h�ABS_PRESSURE�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubh�$ axis
for single-touch touchpads or �����}�(h�$ axis
for single-touch touchpads or �h j�  hhh8Nh:Nubj�  )��}�(h�``ABS_MT_PRESSURE``�h]�h�ABS_MT_PRESSURE�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubh�] on multi-touch capable
touchpads. Some devices can detect multiple fingers but only provide
�����}�(h�] on multi-touch capable
touchpads. Some devices can detect multiple fingers but only provide
�h j�  hhh8Nh:Nubj�  )��}�(h�``ABS_PRESSURE``�h]�h�ABS_PRESSURE�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j�  ubh�.�����}�(hh�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K(h je  hhubh�)��}�(hX  Some devices provide additional touch size information through
the ``ABS_MT_TOUCH_MAJOR/ABS_MT_TOUCH_MINOR`` axes and/or
the ``ABS_MT_WIDTH_MAJOR/ABS_MT_WIDTH_MINOR`` axes. These axes specifcy
the size of the touch ellipse. While the kernel documentation specifies how
these axes are supposed to be mapped, few devices forward reliable
information. libinput uses these values together with a device-specific
:ref:`device-quirks` entry. In other words, touch size detection does not work
unless a device quirk is present for the device.�h]�(h�CSome devices provide additional touch size information through
the �����}�(h�CSome devices provide additional touch size information through
the �h j  hhh8Nh:Nubj�  )��}�(h�)``ABS_MT_TOUCH_MAJOR/ABS_MT_TOUCH_MINOR``�h]�h�%ABS_MT_TOUCH_MAJOR/ABS_MT_TOUCH_MINOR�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  ubh� axes and/or
the �����}�(h� axes and/or
the �h j  hhh8Nh:Nubj�  )��}�(h�)``ABS_MT_WIDTH_MAJOR/ABS_MT_WIDTH_MINOR``�h]�h�%ABS_MT_WIDTH_MAJOR/ABS_MT_WIDTH_MINOR�����}�(hhh j#  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h j  ubh�� axes. These axes specifcy
the size of the touch ellipse. While the kernel documentation specifies how
these axes are supposed to be mapped, few devices forward reliable
information. libinput uses these values together with a device-specific
�����}�(h�� axes. These axes specifcy
the size of the touch ellipse. While the kernel documentation specifies how
these axes are supposed to be mapped, few devices forward reliable
information. libinput uses these values together with a device-specific
�h j  hhh8Nh:Nubh�)��}�(h�:ref:`device-quirks`�h]�h�)��}�(hj8  h]�h�device-quirks�����}�(hhh j:  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j6  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jD  �refexplicit��h��device-quirks�h�h�h��uh0h�h8hkh:K3h j  ubh�k entry. In other words, touch size detection does not work
unless a device quirk is present for the device.�����}�(h�k entry. In other words, touch size detection does not work
unless a device quirk is present for the device.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K3h je  hhubeh!}�(h#]�(�"information-provided-by-the-kernel�jd  eh%]�h']�(�"information provided by the kernel��kernel_pressure_information�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�je  jZ  s�expect_referenced_by_id�}�jd  jZ  subeh!}�(h#]�(�'touchpad-pressure-based-touch-detection�hjeh%]�h']�(�'touchpad pressure-based touch detection��touchpad_pressure�eh)]�h+]�uh0hlh hhhh8hkh:Kjh  }�jr  h_sjj  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ajd  ]�jZ  au�nameids�}�(jr  hjjq  jn  je  jd  jd  ja  u�	nametypes�}�(jr  �jq  Nje  �jd  Nuh#}�(hjhnjn  hnjd  je  ja  je  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�7Hyperlink target "touchpad-pressure" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�AHyperlink target "kernel-pressure-information" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j  �source�hk�line�Kuh0j�  ube�transformer�N�
decoration�Nhhub.